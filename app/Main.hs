{-# LANGUAGE OverloadedStrings #-}
module Main where

import Read
import Processors
import Control.Monad
import Text.Read

getInt :: IO (Maybe Int)
getInt = do
    line <- getLine 
    return $ readMaybe line

options :: [String]
options = [
    "0) for Exit",
    "1) for GET",
    "2) for POST",
    "3) for PUT",
    "4) for DELETE",
    "5) for COPY",
    "6) for MOVE"
    ]

mainLoop :: Address -> IO ()
mainLoop address = do
    forM_ options putStrLn 
    putStrLn "Enter option: "
    option <- getInt
    case option of
        Just 0 -> 
            return ()
        Just 1 -> 
            processGet address >> mainLoop address
        Just 2 -> 
            processPut address >> mainLoop address
        Just 3 -> 
            processPost address >> mainLoop address
        Just 4 -> 
            processDelete address >> mainLoop address
        Just 5 ->
            processCopy address >> mainLoop address
        Just 6 ->
            processMove address >> mainLoop address
        _ ->
            mainLoop address

main :: IO ()
main = do
    address <- readAddress
    mainLoop address