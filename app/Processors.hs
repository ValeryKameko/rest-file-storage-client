{-# LANGUAGE OverloadedStrings #-}
module Processors
    ( processGet,
      processPut,
      processPost,
      processDelete,
      processCopy,
      processMove
    ) where

import Read
import System.IO.Streams (InputStream, OutputStream, stdout)
import qualified System.IO.Streams as Streams
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BC
import Network.Http.Client  
import Text.Printf
import Prelude hiding (catch)
import Control.Exception
import Control.Monad
import Data.Monoid  

takeWhileM :: Monad m => (a -> Bool) -> [m a] -> m [a]
takeWhileM p (ma : mas) = do
    a <- ma
    if p a
        then liftM (a :) $ takeWhileM p mas
        else return []
takeWhileM _ _ = return []

handleGet :: B.ByteString -> Connection -> IO ()
handleGet resourcePath connection  = do
    let request = buildRequest1 $ do
            http GET resourcePath
    sendRequest connection request emptyBody
    receiveResponse connection $ \response inStream ->
        case getStatusCode response of
            200 -> do
                string <- Streams.read inStream
                case string of 
                    Just content -> do
                        putStrLn "OK"
                        putStrLn "Content:"
                        BC.putStrLn content
                    _ -> return ()
            _ -> do
                printf "Error from server %d\n" (getStatusCode response)

handlePut :: B.ByteString -> Connection -> IO ()
handlePut resourcePath connection = do
    let request = buildRequest1 $ do
            http PUT resourcePath
    
    putStrLn "Enter content:"
    content <- liftM BC.unlines $ takeWhileM (not . B.null) (repeat B.getLine)

    inStream <- Streams.fromByteString content
    sendRequest connection request (inputStreamBody inStream)
    receiveResponse connection $ \response inStream ->
        case getStatusCode response of
            200 -> do
                putStrLn "OK"
            _ -> do
                printf "Error from server %d\n" (getStatusCode response)

handlePost :: B.ByteString -> Connection -> IO ()
handlePost resourcePath connection = do
    let request = buildRequest1 $ do
            http POST resourcePath
    
    putStrLn "Enter content:"
    content <- liftM BC.unlines $ takeWhileM (not . B.null) (repeat B.getLine)
    inStream <- Streams.fromByteString content
    sendRequest connection request $ inputStreamBody inStream
    receiveResponse connection $ \response inStream ->
        case getStatusCode response of
            200 -> do
                putStrLn "OK"
            _ -> do
                printf "Error from server %d\n" (getStatusCode response)

handleDelete :: B.ByteString -> Connection -> IO ()
handleDelete resourcePath connection = do
    let request = buildRequest1 $ do
            http DELETE resourcePath
    sendRequest connection request emptyBody
    receiveResponse connection $ \response inStream ->
        case getStatusCode response of
            200 -> do
                putStrLn "OK"
            _ -> do
                printf "Error from server %d\n" (getStatusCode response)

handleCopy :: B.ByteString -> B.ByteString -> Connection -> IO ()
handleCopy resourcePath copyPath connection = do
    let request = buildRequest1 $ do
            http (Method "COPY") $ resourcePath <> "?destination=" <> copyPath
    sendRequest connection request emptyBody
    receiveResponse connection $ \response inStream ->
        case getStatusCode response of
            200 ->
                putStrLn "OK"
            _ ->
                printf "Error from server %d\n" (getStatusCode response)

handleMove :: B.ByteString -> B.ByteString -> Connection -> IO ()
handleMove resourcePath movePath connection = do
    let request = buildRequest1 $ do
            http (Method "MOVE") $ resourcePath <> "?destination=" <> movePath
    sendRequest connection request emptyBody
    receiveResponse connection $ \response inStream ->
        case getStatusCode response of
            200 ->
                putStrLn "OK"
            _ ->
                printf "Error from server %d\n" (getStatusCode response)                

handleConnectionException :: IOException -> IO ()
handleConnectionException exception = do
    putStrLn $ "!!!Connection error!!!"

processGet :: Address -> IO ()
processGet (host, port) = do
    putStrLn "Enter path to resource: "
    resourcePath <- B.getLine
    (withConnection (openConnection host port) $ handleGet resourcePath) `catches` [ Handler handleConnectionException  ]

processPut :: Address -> IO ()
processPut (host, port) = do
    putStrLn "Enter path to resource: "
    resourcePath <- B.getLine
    (withConnection (openConnection host port) $ handlePut resourcePath) `catches` [ Handler handleConnectionException  ]

processPost :: Address -> IO ()
processPost (host, port) = do
    putStrLn "Enter path to resource: "
    resourcePath <- B.getLine
    (withConnection (openConnection host port) $ handlePost resourcePath) `catches` [ Handler handleConnectionException  ]

processDelete :: Address -> IO ()
processDelete (host, port) = do
    putStrLn "Enter path to resource: "
    resourcePath <- B.getLine
    (withConnection (openConnection host port) $ handleDelete resourcePath) `catches` [ Handler handleConnectionException  ]

processCopy :: Address -> IO ()
processCopy (host, port) = do
    putStrLn "Enter path to resource: "
    resourcePath <- B.getLine
    putStrLn "Enter copy path: "
    copyPath <- B.getLine
    (withConnection (openConnection host port) $ handleCopy resourcePath copyPath) `catches` [ Handler handleConnectionException  ]

processMove :: Address -> IO ()
processMove (host, port) = do
    putStrLn "Enter path to resource: "
    resourcePath <- B.getLine
    putStrLn "Enter move path: "
    movePath <- B.getLine
    (withConnection (openConnection host port) $ handleMove resourcePath movePath) `catches` [ Handler handleConnectionException  ]
    