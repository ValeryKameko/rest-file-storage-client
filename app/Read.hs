module Read
    ( readAddress,
      Address
    ) where

import Data.Maybe
import Data.Word
import Text.Read
import Data.List.Split
import Data.ByteString.UTF8 as BU

type Address = (ByteString, Word16)

parseAddress :: String -> Maybe Address
parseAddress string =
    case (splitOn ":" string) of 
        hostname:portString:[] ->
            readMaybe portString >>= (\port -> return (BU.fromString hostname, port))
        _ -> Nothing

readAddress :: IO Address
readAddress = do
    putStrLn "Enter [host:port]:"
    line <- getLine
    case parseAddress line of 
        Just address -> return address
        _ -> readAddress